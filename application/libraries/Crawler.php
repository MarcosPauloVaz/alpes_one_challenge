<?php

require_once('SimpleHtmlDom/Simple_html_dom.php');

class Crawler
{

  const USER_AGENT = "bot";

  public $url; //URL to load DOM from
  public $url_data; //Parsed loaded URL
  public $dom; //DOM structure of loaded URL

  private $robots_rules;

  function __construct()
  {
    $this->set_user_agent(self::USER_AGENT);
    $this->dom = new simple_html_dom();
	}
	
  /* Método para buscar os carros na página SeminovosBH através do Crawler */
  public function get_cars()
  {
		$page_cars = $this->dom->find('div[class=anuncio-container] div[class=anuncio-thumb-new anuncio-card-new]');
		$pages = $this->dom->find('b[class=cor-laranja]');

		if (!$page_cars || $this->dom->find('section[class=nenhum-reseultado]')) {
					return ['message'=> 'Nenhum veículo encontrado!', 'total_pages' => 0, 'page_atual' => 0, 'cars' => []];
		}

		$cars = [];
		foreach($page_cars as $page_car){
			$car = new stdClass();
			$car->id = $page_car->find('div[class=footer-actions] a',0)->getAttribute('data-id');
			$car->name = $page_car->find('h4',0)->innertext;
			$car->description = $page_car->find('div[class=card-description] span b',0)->innertext;
			$car->ano = $page_car->find('div[class=card-detalhes] div[class=ano] span',0)->innertext;
			$car->km = $page_car->find('div[class=card-detalhes] div[class=kilometragem] span',0)->innertext;
			$car->combustivel = $page_car->find('div[class=card-detalhes] div[class=combustivel] span',0)->innertext;
			$car->cambio = $page_car->find('div[class=card-detalhes] div[class=cambio] span',0)->innertext;
			$car->valor = substr($page_car->find('h4[title=Preço do veículo] a',0)->innertext, 4);
			$car->acessorios = [];

			foreach ($page_car->find('div[class=card-acessorios] div[class=acessorio]') as $acessorio) {
				$car->acessorios[] = substr(($acessorio->innertext),33);
			}

			$cars[] = $car;
		}

		return ['message'=> 'successo', 'total_pages' => $pages[1]->innertext, 'page_atual' => $pages[0]->innertext, 'cars' => $cars];

	}
	
	/* Método para buscar detalhes de um carro na página SeminovosBH através do Crawler */
  public function get_car()
  {
		$page_car = $this->dom->find('section[class=veiculo-conteudo]',0);

			if (!$page_car) {
				return ['message'=> 'Veículo não encontrado!'];
			}
			
			$car = new stdClass();
			$car->id = $page_car->find('li[class=item-code] span span[itemprop=sku]',0)->innertext;
			$car->name = $page_car->find('div[class=item-info] h1',0)->innertext;
			$car->description = $page_car->find('div[class=item-info] p[class=desc]',0)->innertext;
			$car->ano = $page_car->find('div[class=item-info] span[itemprop=modelDate]',0)->innertext;
			$car->km = $page_car->find('div[class=item-info] span[itemprop=mileageFromOdometer]',0)->innertext;
			$car->cambio = $page_car->find('div[class=item-info] span[title=Tipo de transmissão]',0)->innertext;
			$car->portas = $page_car->find('div[class=item-info] span[title=Portas]',0)->innertext;
			$car->combustivel = $page_car->find('div[class=item-info] span[itemprop=fuelType]',0)->innertext;
			$car->color = $page_car->find('div[class=item-info] span[itemprop=color]',0)->innertext;
			$car->placa = $page_car->find('div[class=item-info] span[title=Final da placa]',0)->innertext;
			$car->troca = $page_car->find('div[class=item-info] span[title=Aceita troca?]',0)->innertext;
			$car->valor = substr($page_car->find('div[class=item-info] span',0)->innertext, 17);
			$car->acessorios = [];

			foreach ($page_car->find('span[class=description-print]') as $acessorio) {
				$car->acessorios[] = $acessorio->innertext;
			}


		return ['message'=> 'successo', 'car' => $car];

	}
	
  /**
   * Set the user agent to be used for all cURL calls
   *
   * @param 	string	user agent string
   * @return 	void
   */
  public function set_user_agent($user_agent)
  {
    ini_set('user_agent', $user_agent);
  }

  /**
   * Check to make sure URL is valid
   *
   * @param 	string	URL to check
   * @return	boolean	True if URL is valid. False is url is not valid.
   */
  private function check_url($url)
  {
    $headers = @get_headers($url, 0);
    if (is_array($headers)) {
      if (strpos($headers[0], '404')) {
        return false;
      }

      foreach ($headers as $header) {
        if (strpos($header, '404 Not Found')) {
          return false;
        }
      }

      return true;
    } else {
      return false;
    }
  }

  /**
   * Set URL to scrape/crawl.
   *
   * @param 	string 	URL to crawl
   * @return	boolean	True if URL is valid. False is URL is not valid
   */
  public function set_url($url)
  {
    $this->url = $url;

    if ((strpos($url, 'http')) === false) $url = 'http://' . $url;

    if ($this->check_url($url) === false) {
      return false;
    }

    if ($this->dom->load_file($url) === false) {
      return false;
    }

    $this->url_data = parse_url($url);
    if (empty($this->url_data['scheme'])) {
      $this->data['scheme'] == 'http';
    }
    $this->url_data['domain'] = implode(".", array_slice(explode(".", $this->url_data['host']), -2));

    if (empty($this->url_data['path']) || $this->url_data['path'] != '/robots.txt') {
      $this->get_robots();
    }

    return true;
  }

  /**
   * Retrieve and parse the loaded URL's robots.txt
   *
   * @return	array/boolean	Returns array of rules if robots.txt is valid. Otherwise returns True if no rules exist or False if robots.txt is not valid.
   */
  private function get_robots()
  {
    if (empty($this->url_data)) return false;

    $robots_url = 'http://' . $this->url_data['domain'] . '/robots.txt';

    if (!$this->check_url($robots_url)) {
      return false;
    }

    $robots_text = @file($robots_url);

    if (empty($robots_text)) {
      $this->robots_rules = false;
      return;
    }

    $user_agents = implode("|", array(preg_quote('*'), preg_quote(self::USER_AGENT)));

    $this->robots_rules = array();

    foreach ($robots_text as $line) {
      if (!$line = trim($line)) continue;

      if (preg_match('/^\s*User-agent: (.*)/i', $line, $match)) {
        $ruleApplies = preg_match("/($user_agents)/i", $match[1]);
      }
      if (!empty($ruleApplies) && preg_match('/^\s*Disallow:(.*)/i', $line, $regs)) {
        // an empty rule implies full access - no further tests required
        if (!$regs[1]) return true;
        // add rules that apply to array for testing
        $this->robots_rules[] = preg_quote(trim($regs[1]), '/');
      }
    }

    return $this->robots_rules;
  }

}

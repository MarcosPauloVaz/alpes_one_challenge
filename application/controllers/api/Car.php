<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car extends CI_Controller {

	private $base_url_seminovos = "https://seminovos.com.br";

	public function __construct()
	{
		parent::__construct();
		header('Content-Type: application/json');
		$this->load->library('crawler');
	}

	public function get_list_car(){

		//Parâmetros recebidos via POST
		$marca = $this->input->post('marca');
		$modelo = $this->input->post('modelo');
		$versao = $this->input->post('versao');
		$ano_de = $this->input->post('ano_de');
		$ano_ate = $this->input->post('ano_ate');
		$preco_de = $this->input->post('preco_de');
		$preco_ate = $this->input->post('preco_ate');
		$km_de = $this->input->post('km_de');
		$km_ate = $this->input->post('km_ate');
		$combustivel = $this->input->post('combustivel');

		$url = $this->base_url_seminovos."/carro";

		/* Construção da URL do site seminovosBH */
		$url .= $marca ? "/".$marca : "";
		$url .= $modelo ? "/".$modelo : "";
		$url .= $versao ? "/versao-".$versao : "";
		$url .= $combustivel ? "/combustivel-".$this->getIdCombustivel($combustivel) : "";
		$url .= $this->getUrlInterval($ano_de, $ano_ate, 'ano');
		$url .= $this->getUrlInterval($preco_de, $preco_ate, 'preco');
		$url .= $this->getUrlInterval($km_de, $km_ate, 'km');
		/* FIM Construção da URL do site seminovosBH */

		//Chamada para método que aciona o Crawler e retorna os dados da página SeminovosBH
		$site_data = $this->crawler($url, 'list_cars');

		echo json_encode($site_data);
	}
	
	public function get_car($id){

		$url = $this->base_url_seminovos."/".$id;

		//Chamada para método que aciona o Crawler e retorna os dados da página SeminovosBH
		$site_data = $this->crawler($url, 'car');


		echo json_encode($site_data);
	}

	private function crawler($site_url, $item){
		//Import da library Crawler (libraries/Crawler)
		$this->load->library('crawler');

		$site_data = array();

		if($this->crawler->set_url($site_url) !== false){
			//Invocação dos métodos na library Crawler
			switch ($item) {
				case 'list_cars':
					$site_data = $this->crawler->get_cars();
					break;
				case 'car':
					$site_data = $this->crawler->get_car();
					break;
				default:
					return false;
					break;
			}

			return $site_data;
		}
		else{
			return false;
		}
	}

	/** Método que converte a string do combustível no id do portal Seminovos BH */
	public function getIdCombustivel($combustivel){
		switch ($combustivel) {
			case 'alcool':
				return 1;
				break;
			case 'alcool+kitgas':
				return 9;
				break;
			case 'bicombustivel':
				return 2;
				break;
			case 'diesel':
				return 3;
				break;
			case 'gasolina':
				return 4;
				break;
			case 'gasolina+eletrico':
				return 8;
				break;
			case 'gasolina+kitgas':
				return 5;
				break;
			case 'kitgas':
				return 6;
				break;
			case 'tetrafuel':
				return 7;
				break;
			default:
				return false;
				break;
		}
	}

	/** Método que monta forma correta da URL quando os parâmetros forem intervalo */
	public function getUrlInterval($de, $ate, $item){
		return ($de || $ate) ? "/".$item."-" .
		($de ? $de."-" : "-") . 
		($ate ? $ate : "-") : "";
	}
}

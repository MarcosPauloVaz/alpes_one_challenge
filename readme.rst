###################
API SeminovosBH Alpes One
###################

*******************


*******************
Introdução
*******************

Bem-vindo a API SeminovosBH Alpes One, desenvolvida para extrair dados do portal www.seminovosbh.com.br de forma simples.

PHP 7.4 | Codeigniter 3.1.11 | Hospedada na AWS Cloud (EC2)

Autor: Marcos Paulo de Souza Vaz


*******************

Documentação
*******************

Link da Documentação da API: http://docapiseminovosbhalpesone.s3-website-us-east-1.amazonaws.com
